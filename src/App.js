import React, {useEffect, useState} from 'react';
import {Route, Switch, useHistory, Redirect} from 'react-router-dom';
import './App.css';
import Home from './routes/home/home';
import Login from './routes/login/login';
import Header from './components/header';
import Register from './routes/register/register';
import axios from 'axios';
import CreateBetForm from './routes/create/createBetForm';
import Profile from './routes/profile/profile';
import UserBets from './routes/userbets/userBets';
import Admin from './routes/admin/admin';

const useSemiPersistentState = (key, initialState) => {
    const [value, setValue] = React.useState(
        localStorage.getItem(key) || initialState
    );

    useEffect(() => {
        localStorage.setItem(key, value);
    }, [value, key]);

    return [value, setValue];
};

const App = () => {

    const [jwtToken, setJwtToken] = useSemiPersistentState('jwt', '');
    const [user, setUser] = useState({loggedIn: false, jwt: jwtToken});
    const [userData, setUserData] = useState({});
    const history = useHistory();

    axios.interceptors.request.use(function (config) {
        if (jwtToken !== '') {
            config.headers.Authorization = `Bearer ${jwtToken}`;
        }
        if (config.url.includes('login') || config.url.includes('register')) {
            config.headers.Authorization = '';
        }
        return config;
    });

    axios.interceptors.response.use(response => {
        if (response.status === 401) {
            history.push('/');
        }
        return response;
    });

    const loadPerson = () => {
        axios
            .get(`/api/user/me`, {})
            .then(function (response) {
                setUserData(response.data);
            })
            .catch(function (error) {
            });
    };

    const success = (jwtToken) => {
        setUser({...user, loggedIn: true, jwt: jwtToken});
        setJwtToken(jwtToken);
        loadPerson();
    };

    const handleSuccessfulAuth = (jwtToken) => {
        success(jwtToken);
    };

    const handleSuccessfulRegister = (jwtToken) => {
        success(jwtToken);
    };

    const handleLogoutRequest = () => {
        setUser({loggedIn: false, jwt: ''});
        setJwtToken('');
        setUserData({});
        window.location.reload();
    };

    const handleBetJoin = () => {
        loadPerson();
    }

    const handleSuccessfulCreate = () => {
        loadPerson();
    };

    useEffect(() => {
        if (jwtToken !== '') {
            axios
                .get(`/api/user/loggedIn`, {})
                .then(function (response) {
                    setUser({...user, loggedIn: true});
                    loadPerson();
                })
                .catch(function (error) {
                    setUser({...user, loggedIn: false});
                    setJwtToken('');
                });
        } else {
            history.push('/');
        }
    }, [jwtToken]);

    return (
        <div className="App">

            {user.loggedIn && (
                <Header user={user} userData={userData} handleLogoutRequest={handleLogoutRequest}/>
            )}
            <Switch>
                <Route path="/login">
                    <Login handleSuccessfulAuth={handleSuccessfulAuth}/>
                </Route>
                <Route path="/register">
                    <Register handleSuccessfulRegister={handleSuccessfulRegister}/>
                </Route>
                <Route path="/create">
                    <CreateBetForm handleSuccessfulCreate={handleSuccessfulCreate} user={user}/>
                </Route>
                <Route path="/profile">
                    <Profile userData={userData}/>
                </Route>
                <Route path="/userbets">
                    <UserBets userData={userData}/>
                </Route>
                <Route path="/admin">
                    {userData.admin
                        ? <Admin userData={userData}/>
                        : <Redirect to={{pathname: "/"}} />
                    }
                </Route>
                <Route path="/">
                    <Home user={user} userData={userData} handleBetJoin={handleBetJoin}/>
                </Route>
            </Switch>
        </div>
    );
};

export default App;
