import {ReactNode} from 'react';
import {
    Box,
    Button,
    Flex,
    HStack,
    IconButton,
    Image,
    Menu,
    MenuButton,
    MenuDivider,
    MenuItem,
    MenuList,
    Stack,
    Text,
    useColorModeValue,
    useDisclosure
} from '@chakra-ui/react';
import {GiHamburgerMenu} from 'react-icons/gi';
import {GrClose} from 'react-icons/gr';
import {FaUserAlt} from 'react-icons/fa';
import {Link} from 'react-router-dom';
import headerLogo from '../assets/logo_transparent.png';

const Links = ['Home'];

const NavLink = ({children}: { children: ReactNode }) => (
    <Link
        px={2}
        py={1}
        rounded={'md'}
        _hover={{
            textDecoration: 'none',
            bg: useColorModeValue('gray.200', 'gray.700'),
        }}
        to=""
    >
        {children}
    </Link>
);

const Header = ({user, handleLogoutRequest, userData}) => {
    const {isOpen, onOpen, onClose} = useDisclosure();

    return (
        <>
            <Box bg={useColorModeValue('gray.100', 'gray.900')} px={4}>
                <Flex h={16} alignItems={'center'} justifyContent={'space-between'}>
                    <IconButton
                        size={'md'}
                        icon={isOpen ? <GrClose/> : <GiHamburgerMenu/>}
                        aria-label={'Open Menu'}
                        display={{md: !isOpen ? 'none' : 'inherit'}}
                        onClick={isOpen ? onClose : onOpen}
                    />
                    <HStack spacing={8} alignItems={'center'}>
                        <Box>
                            <Link to="/"><Image boxSize="4rem" src={headerLogo} alt="Logo"/></Link>
                        </Box>
                        <HStack as={'nav'} spacing={4} display={{base: 'none', md: 'flex'}}>
                            {Links.map((link) => (
                                <NavLink key={link}>{link}</NavLink>
                            ))}
                        </HStack>
                    </HStack>
                    <Flex alignItems={'center'}>
                        {user.loggedIn && (
                            <Text mr={10}>Balance: {userData.balance}</Text>
                        )}
                        {user.loggedIn && (
                            <Button
                                variant={'solid'}
                                colorScheme={'teal'}
                                size={'sm'}
                                display={user.loggedIn}
                                mr={4}
                            >
                                <Link to="/create">Create {user.loggedIn}</Link>
                            </Button>
                        )}
                        {!user.loggedIn && (
                            <Button
                                variant={'solid'}
                                colorScheme={'teal'}
                                size={'sm'}
                                display={user.loggedIn}
                                mr={4}
                            >
                                <Link to="/login">Login</Link>
                            </Button>
                        )}
                        {!user.loggedIn && (
                            <Button variant={'solid'} colorScheme={'teal'} size={'sm'} mr={4}>
                                <Link to="/register">Sign Up</Link>
                            </Button>
                        )}

                        {user.loggedIn && (
                            <Menu>
                                <MenuButton as={Button} rounded={'full'} variant={'link'} cursor={'pointer'}>
                                    <FaUserAlt/>
                                </MenuButton>
                                <MenuList>
                                    <Link to="/profile"><MenuItem>Profile</MenuItem></Link>
                                    <Link to="/userbets"><MenuItem>My Bets</MenuItem></Link>
                                    {userData.admin && (
                                        <Link to="/admin"><MenuItem>Admin</MenuItem></Link>
                                    )}
                                    <MenuDivider/>
                                    <MenuItem onClick={handleLogoutRequest}>Logout</MenuItem>
                                </MenuList>
                            </Menu>
                        )}
                    </Flex>
                </Flex>

                {isOpen ? (
                    <Box pb={4}>
                        <Stack as={'nav'} spacing={4}>
                            {Links.map((link) => (
                                <NavLink key={link}>{link}</NavLink>
                            ))}
                        </Stack>
                    </Box>
                ) : null}
            </Box>
        </>
    );
};

export default Header;
