import {
    Accordion,
    AccordionButton,
    AccordionIcon,
    AccordionItem,
    AccordionPanel,
    Box,
    Button,
    Flex,
    Text,
} from '@chakra-ui/react';

const BetList = ({bets, handleBetJoinRequest, userData, admin, handleWinEvolution}) => {

    return (
        <Flex
            align={'center'}
            justify={'center'}
        >
            <Accordion w={600} allowMultiple>
                {bets && bets.sort((a, b) => new Date(b.creationDate) - new Date(a.creationDate)).map((bet) => (
                    <AccordionItem key={bet.betId}>
                        <h2>
                            <AccordionButton>
                                <Box flex="1" textAlign="left">
                                    {bet.title}
                                </Box>
                                <AccordionIcon/>
                            </AccordionButton>
                        </h2>
                        <AccordionPanel textAlign="left">

                            <Text>Description: {bet.description}</Text>
                            <Text>Win Condition: {bet.winCondition}</Text>
                            <Text>Number of participants: {bet.numberOfParticipants}</Text>
                            <Text>Category: {bet.category}</Text>
                            <Text>You can win: {bet.betterAmount} Coins</Text>
                            <Text>Creation Date: {new Date(bet.creationDate).toLocaleDateString()}</Text>
                            <Text>Open Until: {new Date(bet.joinUntil).toLocaleDateString()}</Text>
                            {bet.canUserJoin && (
                                <Button onClick={() => handleBetJoinRequest(bet)}
                                        bg={'#00703d'}
                                        _hover={{bg: '#019654'}}
                                        mt={4}
                                >{bet.joinerAmount} Coins to join this bet</Button>)
                            }
                            {userData.admin && admin && (
                                <>
                                    <Button onClick={() => handleWinEvolution('creator', bet)}
                                            bg={'#00703d'}
                                            _hover={{bg: '#019654'}}
                                            mt={4}
                                            mr={4}
                                    >Creator Wins</Button>
                                    <Button onClick={() => handleWinEvolution('joiners', bet)}
                                            bg={'#00703d'}
                                            _hover={{bg: '#019654'}}
                                            mt={4}
                                    >Joiners Wins</Button>
                                </>
                            )}
                        </AccordionPanel>
                    </AccordionItem>
                ))}
            </Accordion>
        </Flex>
    );
};

export default BetList;
