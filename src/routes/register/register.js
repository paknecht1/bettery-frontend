import React, {useRef} from 'react';
import {
    Box,
    Button,
    CircularProgress,
    Flex,
    FormControl,
    FormErrorMessage,
    FormLabel,
    Heading,
    Input,
    Stack,
    useColorModeValue,
    useToast,
} from '@chakra-ui/react';
import * as Yup from 'yup';
import {Field, Form, Formik} from 'formik';
import axios from 'axios';
import {useHistory} from 'react-router-dom';

const Register = ({handleSuccessfulRegister}) => {
    const history = useHistory();

    const failToast = useToast();
    const toastFailRef = useRef();

    const successToast = useToast();
    const toastSuccessRef = useRef();

    let formFields = {
        firstName: '',
        lastName: '',
        username: '',
        email: '',
        password: '',
        passwordConfirmation: '',
    };

    const ContactFormSchema = Yup.object().shape({
        firstName: Yup.string()
            .min(2, 'Your first name should consist of at least two characters')
            .max(50, 'Your first name should not have more than 50 characters')
            .required('Please enter your first name'),
        lastName: Yup.string()
            .min(2, 'Your last name should consist of at least two characters')
            .max(50, 'Your last name should not have more than 50 characters')
            .required('Please enter your last name'),
        username: Yup.string()
            .min(2, 'Your username should consist of at least two characters')
            .max(50, 'Your username should not have more than 50 characters')
            .required('Please enter your username'),
        email: Yup.string()
            .min(2, 'Your email should consist of at least two characters')
            .max(50, 'Your email should not have more than 50 characters')
            .required('Please enter your email'),
        password: Yup.string()
            .min(10, 'Your password should consist of at least ten characters')
            .max(50, 'Your password should not have more than 50 characters')
            .required('Please enter your password')
            .matches(
                /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/,
                'Must Contain 10 Characters, One Uppercase, One Lowercase, One Number and one special case Character'
            ),
        passwordConfirmation: Yup.string().oneOf([Yup.ref('password'), null], 'Passwords must match'),
    });

    const handleSubmit = (values, actions) => {
        axios
            .post(`/api/user/register`, {
                firstName: values.firstName,
                lastName: values.lastName,
                username: values.username,
                email: values.email,
                password: values.password,
            })
            .then(function (response) {
                // handle success
                toastSuccessRef.current = successToast({
                    title: 'Registration successful',
                    description: 'You will be redirected shortly',
                    status: 'success',
                    duration: 6000,
                    isClosable: true,
                });
                actions.setSubmitting(false);
                handleSuccessfulRegister(response.data);
                setTimeout(function () {
                    history.push('/');
                }, 1500);
            })
            .catch(function (error) {

                if (error.response.status === 422 || error.response.status === 400) {
                    if (error.response.data && error.response.data.fieldError && error.response.data.fieldError.length > 0) {
                        error.response.data.fieldError.forEach((error) => {
                            actions.setFieldError(error.field, error.message);
                        });
                    }
                } else {
                    toastFailRef.current = failToast({
                        title: 'Registration not successful',
                        description: 'Something went wrong',
                        status: 'error',
                        duration: 6000,
                        isClosable: true,
                    });
                    console.log(error);
                }
                actions.setSubmitting(false);
            });
    };

    return (
        <Flex
            minH={'100vh'}
            align={'center'}
            justify={'center'}
            bg={useColorModeValue('gray.50', 'gray.800')}
        >
            <Stack spacing={8} mx={'auto'} maxW={'lg'} w={700} py={12} px={6}>
                <Stack align={'center'}>
                    <Heading fontSize={'4xl'}>Register now</Heading>
                </Stack>
                <Box rounded={'lg'} bg={useColorModeValue('white', 'gray.700')} boxShadow={'lg'} p={8}>
                    <Stack spacing={4}>
                        <Formik
                            initialValues={formFields}
                            validationSchema={ContactFormSchema}
                            onSubmit={handleSubmit}
                        >
                            {(props) => (
                                <Form>
                                    <Field name="firstName">
                                        {({field, form}) => (
                                            <FormControl
                                                isInvalid={form.errors.firstName && form.touched.firstName}
                                                mt={4}
                                            >
                                                <FormLabel htmlFor="firstName">First Name</FormLabel>
                                                <Input
                                                    {...field}
                                                    type="text"
                                                    size="lg"
                                                    id="firstName"
                                                    placeholder="First Name"
                                                />
                                                <FormErrorMessage>{form.errors.firstName}</FormErrorMessage>
                                            </FormControl>
                                        )}
                                    </Field>
                                    <Field name="lastName">
                                        {({field, form}) => (
                                            <FormControl isInvalid={form.errors.lastName && form.touched.lastName}
                                                         mt={4}>
                                                <FormLabel htmlFor="lastName">Last Name</FormLabel>
                                                <Input
                                                    {...field}
                                                    type="text"
                                                    size="lg"
                                                    id="lastName"
                                                    placeholder="Last Name"
                                                />
                                                <FormErrorMessage>{form.errors.lastName}</FormErrorMessage>
                                            </FormControl>
                                        )}
                                    </Field>
                                    <Field name="username">
                                        {({field, form}) => (
                                            <FormControl isInvalid={form.errors.username && form.touched.username}
                                                         mt={4}>
                                                <FormLabel htmlFor="username">Username</FormLabel>
                                                <Input
                                                    {...field}
                                                    type="text"
                                                    size="lg"
                                                    id="username"
                                                    placeholder="Username"
                                                />
                                                <FormErrorMessage>{form.errors.username}</FormErrorMessage>
                                            </FormControl>
                                        )}
                                    </Field>
                                    <Field name="email">
                                        {({field, form}) => (
                                            <FormControl isInvalid={form.errors.email && form.touched.email} mt={4}>
                                                <FormLabel htmlFor="email">Email</FormLabel>
                                                <Input {...field} type="email" size="lg" id="email"
                                                       placeholder="Email"/>
                                                <FormErrorMessage>{form.errors.email}</FormErrorMessage>
                                            </FormControl>
                                        )}
                                    </Field>
                                    <Field name="password">
                                        {({field, form}) => (
                                            <FormControl isInvalid={form.errors.password && form.touched.password}
                                                         mt={4}>
                                                <FormLabel htmlFor="password">Password</FormLabel>
                                                <Input
                                                    {...field}
                                                    type="password"
                                                    size="lg"
                                                    id="password"
                                                    placeholder="password"
                                                />
                                                <FormErrorMessage>{form.errors.password}</FormErrorMessage>
                                            </FormControl>
                                        )}
                                    </Field>
                                    <Field name="passwordConfirmation">
                                        {({field, form}) => (
                                            <FormControl
                                                isInvalid={
                                                    form.errors.passwordConfirmation && form.touched.passwordConfirmation
                                                }
                                                mt={4}
                                            >
                                                <FormLabel htmlFor="passwordConfirmation">Password
                                                    Confirmation</FormLabel>
                                                <Input
                                                    {...field}
                                                    type="password"
                                                    size="lg"
                                                    id="passwordConfirmation"
                                                    placeholder="Password Confirmation"
                                                />
                                                <FormErrorMessage>{form.errors.passwordConfirmation}</FormErrorMessage>
                                            </FormControl>
                                        )}
                                    </Field>
                                    <Stack spacing={10}>
                                        <Button
                                            type="submit"
                                            bg={'#00703d'}
                                            _hover={{bg: '#019654'}}
                                            mt={4}
                                        >
                                            {props.isSubmitting ? (
                                                <CircularProgress isIndeterminate size="24px" color="black"/>
                                            ) : (
                                                'Absenden'
                                            )}
                                        </Button>
                                    </Stack>
                                    <Stack spacing={10}>
                                        <Button
                                            onClick={() => history.push('/')}
                                            bg={'#8b928f'}
                                            _hover={{bg: '#777d7a'}}
                                            mt={4}
                                        >
                                            Back to Overview
                                        </Button>
                                    </Stack>
                                </Form>
                            )}
                        </Formik>
                    </Stack>
                </Box>
            </Stack>
        </Flex>
    );
};

export default Register;
