import React, {useRef, useState} from 'react';
import {
    Box,
    Button,
    CircularProgress,
    Flex,
    FormControl,
    FormErrorMessage,
    FormLabel,
    Heading,
    Input,
    NumberDecrementStepper,
    NumberIncrementStepper,
    NumberInput,
    NumberInputField,
    NumberInputStepper,
    Stack,
    Textarea,
    useColorModeValue,
    useToast,
} from '@chakra-ui/react';
import * as Yup from 'yup';
import {Field, Form, Formik, useField, useFormikContext} from 'formik';
import axios from 'axios';
import {useHistory} from 'react-router-dom';
import {DatePicker} from '../../components/datePicker';


const DatePickerField = ({...props}) => {
    const {setFieldValue} = useFormikContext();
    const [field] = useField(props);
    return (
        <DatePicker
            {...field}
            {...props}
            selected={(field.value && new Date(field.value)) || null}
            onChange={val => {
                setFieldValue(field.name, val);
            }}
        />
    );
};

const CreateBetForm = ({handleSuccessfulCreate, user}) => {
    const history = useHistory();

    const failToast = useToast();
    const toastFailRef = useRef();

    const successToast = useToast();
    const toastSuccessRef = useRef();

    let formFields = {
        title: '',
        numberOfParticipants: '',
        creatorAmount: '',
        joineeAmount: '',
        wincondition: '',
        openUntil: '',
        evaluatedAt: '',
        category: '',
        description: '',
    };

    const [openUntilDate, setOpenUntilDate] = useState(new Date());
    const [evaluatedAtDate, setEvaluatedAtDate] = useState(new Date());

    const ContactFormSchema = Yup.object().shape(
        {
            title: Yup.string()
                .min(1, 'Title should at least be 2 Characters')
                .max(30, 'Your Title should not have more than 30 characters')
                .required('Please enter a Title for your bet'),
            joineeAmount: Yup
                .string().required('Enter amount to join bet'),
            creatorAmount: Yup
                .string().required('Enter amount you are betting'),
            numberOfParticipants: Yup
                .string().required('Enter how many people can join the bet'),
            wincondition: Yup.string()
                .min(4, 'Please enter more then one word')
                .max(50, 'Wincondition shouldnt be more then 50 characters')
                .required('Please enter a wincondition'),
            category: Yup.string()
                .min(2, 'Bet category must be more then 2 characters')
                .max(20, 'Category cannot be more then 20 characters')
                .required('Please enter to what category your bet belongs'),
            description: Yup.string()
                .min(5, 'Your description should be more then 5 characters')
                .max(250, 'Keep your description under 250 characters')
                .required('Please describe your bet'),
        });

    const handleSubmit = (values, actions) => {
        axios.post('/api/bets/create', {
                title: values.title,
                numberOfParticipants: values.numberOfParticipants,
                description: values.description,
                category: values.category,
                betterAmount: values.creatorAmount,
                joinerAmount: values.joineeAmount,
                creator: values.creator,
                winCondition: values.wincondition,
                joinUntil: values.openUntil,
                evaluatedAt: values.evaluatedAt
            },
            {
                headers: {
                    'Authorization': `Bearer ${user.jwt}`
                }
            })
            .then(function (response) {
                toastSuccessRef.current = successToast({
                    title: 'Bet created successfully',
                    description: 'You will be redirected to the Dashboard shortly',
                    status: 'success',
                    duration: 6000,
                    isClosable: true,
                });
                actions.setSubmitting(false);
                handleSuccessfulCreate();
                setTimeout(function () {
                    history.push('/');
                }, 1500);
            }).catch(function (error) {
            if (error.response.status == 403) {
                console.log(error.response.data);
                toastFailRef.current = failToast({
                    title: 'Creating Bet was not successfull',
                    description: error.response.data,
                    status: 'error',
                    duration: 6000,
                    isClosable: true,
                });
            }
            actions.setSubmitting(false);
        });
    };

    return (
        <Flex
            minH={'100vh'}
            align={'center'}
            justify={'center'}
            bg={useColorModeValue('gray.50', 'gray.800')}
        >
            <Stack spacing={8} mx={'auto'} maxW={'lg'} w={700} py={12} px={6}>
                <Stack align={'center'}>
                    <Heading fontSize={'4xl'}>Create a new BET</Heading>
                </Stack>
                <Box rounded={'lg'} bg={useColorModeValue('white', 'gray.700')} boxShadow={'lg'} p={8}>
                    <Stack spacing={4}>
                        <Formik
                            initialValues={formFields}
                            validationSchema={ContactFormSchema}
                            onSubmit={handleSubmit}
                        >
                            {(props) => (
                                <Form>
                                    <Field name="title">
                                        {({field, form}) => (
                                            <FormControl
                                                isInvalid={form.errors.title && form.touched.title}
                                                mt={4}
                                            >
                                                <FormLabel htmlFor="title">Title</FormLabel>
                                                <Input
                                                    {...field}
                                                    type="text"
                                                    size="lg"
                                                    id="title"
                                                    placeholder="FCA vs FCB"
                                                />
                                                <FormErrorMessage>{form.errors.title}</FormErrorMessage>
                                            </FormControl>
                                        )}
                                    </Field>
                                    <Field name="numberOfParticipants">
                                        {({field, form}) => (
                                            <FormControl
                                                isInvalid={form.errors.numberOfParticipants && form.touched.numberOfParticipants}
                                                mt={4}>
                                                <FormLabel htmlFor="numberOfParticipants">Number Of
                                                    Participants</FormLabel>
                                                <NumberInput onChange={val => form.setFieldValue(field.name, val)}
                                                             min={1} max={50}
                                                             id="numberOfParticipants">
                                                    <NumberInputField/>
                                                    <NumberInputStepper>
                                                        <NumberIncrementStepper/>
                                                        <NumberDecrementStepper/>
                                                    </NumberInputStepper>
                                                </NumberInput>
                                                <FormErrorMessage>{form.errors.numberOfParticipants}</FormErrorMessage>
                                            </FormControl>
                                        )}
                                    </Field>
                                    <Field name="creatorAmount">
                                        {({field, form}) => (
                                            <FormControl
                                                isInvalid={form.errors.creatorAmount && form.touched.creatorAmount}
                                                mt={4}>
                                                <FormLabel htmlFor="creatorAmount">How much are YOU betting</FormLabel>
                                                <NumberInput onChange={val => form.setFieldValue(field.name, val)}
                                                             min={1} max={10000}
                                                             id="creatorAmount">
                                                    <NumberInputField/>
                                                    <NumberInputStepper>
                                                        <NumberIncrementStepper/>
                                                        <NumberDecrementStepper/>
                                                    </NumberInputStepper>
                                                </NumberInput>
                                                <FormErrorMessage>{form.errors.creatorAmount}</FormErrorMessage>
                                            </FormControl>
                                        )}
                                    </Field>
                                    <Field name="joineeAmount">
                                        {({field, form}) => (
                                            <FormControl
                                                isInvalid={form.errors.joineeAmount && form.touched.joineeAmount}
                                                mt={4}>
                                                <FormLabel htmlFor="joineeAmount">How much for someone to join the
                                                    Bet</FormLabel>
                                                <NumberInput onChange={val => form.setFieldValue(field.name, val)}
                                                             min={1} max={10000}
                                                             id="creatorAmount">
                                                    <NumberInputField/>
                                                    <NumberInputStepper>
                                                        <NumberIncrementStepper/>
                                                        <NumberDecrementStepper/>
                                                    </NumberInputStepper>
                                                </NumberInput>
                                                <FormErrorMessage>{form.errors.joineeAmount}</FormErrorMessage>
                                            </FormControl>
                                        )}
                                    </Field>
                                    <Field name="wincondition">
                                        {({field, form}) => (
                                            <FormControl
                                                isInvalid={form.errors.wincondition && form.touched.wincondition}
                                                mt={4}
                                            >
                                                <FormLabel htmlFor="wincondition">Describe who wins under what
                                                    condition</FormLabel>
                                                <Textarea
                                                    {...field}
                                                    type="text"
                                                    size="lg"
                                                    id="category"
                                                    placeholder="FCA wins"
                                                />
                                                <FormErrorMessage>{form.errors.wincondition}</FormErrorMessage>
                                            </FormControl>
                                        )}
                                    </Field>
                                    <Field name="openUntil">
                                        {({field, form}) => (
                                            <FormControl
                                                isInvalid={form.errors.openUntil && form.touched.openUntil}
                                                mt={4}>
                                                <FormLabel htmlFor="openUntil">Open Until</FormLabel>
                                                <DatePicker
                                                    id="openUntil"
                                                    selectedDate={openUntilDate}
                                                    onChange={function (openUntilDate) {
                                                        setOpenUntilDate(openUntilDate);
                                                        form.setFieldValue(field.name, openUntilDate.toJSON());
                                                    }
                                                    }
                                                    showPopperArrow={true}
                                                />
                                                <FormErrorMessage>{form.errors.openUntil}</FormErrorMessage>

                                            </FormControl>
                                        )}
                                    </Field>
                                    <Field name="evaluatedAt">
                                        {({field, form}) => (
                                            <FormControl
                                                isInvalid={form.errors.evaluatedAt && form.touched.evaluatedAt}
                                                mt={4}>
                                                <FormLabel htmlFor="evaluatedAt">Evaluated At</FormLabel>
                                                <DatePicker
                                                    id="evaluatedAt"
                                                    selectedDate={evaluatedAtDate}
                                                    onChange={function (evaluatedAtDate) {
                                                        setEvaluatedAtDate(evaluatedAtDate);
                                                        form.setFieldValue(field.name, evaluatedAtDate.toJSON());
                                                    }
                                                    }
                                                    showPopperArrow={true}
                                                />
                                                <FormErrorMessage>{form.errors.evaluatedAt}</FormErrorMessage>

                                            </FormControl>
                                        )}
                                    </Field>
                                    <Field name="category">
                                        {({field, form}) => (
                                            <FormControl
                                                isInvalid={form.errors.category && form.touched.category}
                                                mt={4}
                                            >
                                                <FormLabel htmlFor="category">What type of bet is it</FormLabel>
                                                <Input
                                                    {...field}
                                                    type="text"
                                                    size="lg"
                                                    id="category"
                                                    placeholder="Football"
                                                />
                                                <FormErrorMessage>{form.errors.category}</FormErrorMessage>
                                            </FormControl>
                                        )}
                                    </Field>
                                    <Field name="description">
                                        {({field, form}) => (
                                            <FormControl
                                                isInvalid={form.errors.description && form.touched.description}
                                                mt={4}
                                            >
                                                <FormLabel htmlFor="description">Describe your Bet</FormLabel>
                                                <Textarea
                                                    {...field}
                                                    type="text"
                                                    size="lg"
                                                    id="category"
                                                    placeholder="Football match between FCA and FCB next saturday"
                                                />
                                                <FormErrorMessage>{form.errors.description}</FormErrorMessage>
                                            </FormControl>
                                        )}
                                    </Field>
                                    <Stack spacing={10}>
                                        <Button
                                            type="submit"
                                            bg={'blue.400'}
                                            color={'white'}
                                            _hover={{
                                                bg: 'blue.500',
                                            }}
                                            mt={4}
                                        >
                                            {props.isSubmitting ? (
                                                <CircularProgress isIndeterminate size="24px" color="black"/>
                                            ) : (
                                                'Create'
                                            )}
                                        </Button>
                                    </Stack>
                                </Form>
                            )}
                        </Formik>
                    </Stack>
                </Box>
            </Stack>
        </Flex>
    );
};

export default CreateBetForm;
