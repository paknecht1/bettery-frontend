import React, {useEffect, useState} from 'react';
import axios from 'axios';
import {Heading} from '@chakra-ui/react';
import BetList from '../../components/betList';

const Admin = ({userData}) => {

    const [checkBets, setCheckBets] = useState([]);

    const loaBetsToCheck = () => {
        axios
            .get(`/api/bets/evaluate/list`, {})
            .then(function (response) {
                setCheckBets(response.data);
            })
            .catch(function (error) {
            });
    };

    useEffect(() => {
        loaBetsToCheck();
    }, []);

    const handleWinEvolution = (winner, bet) => {
        let creatorWins = false;
        if (winner === 'creator') {
            creatorWins = true;
        }
        axios
            .put(`/api/bets/resolve/${bet.betId}/${creatorWins}`, {})
            .then(function (response) {
                console.log(response);
                loaBetsToCheck();
            })
            .catch(function (error) {
            });
    }

    return (
        <>
            <Heading
                fontWeight={600}
                fontSize={{base: '1xl', sm: '2xl', md: '4xl'}}
                lineHeight={'110%'}>
                Bets to resolve
            </Heading>
            <BetList bets={checkBets} userData={userData} handleWinEvolution={handleWinEvolution} admin={true}/>
        </>
    )
}

export default Admin;
