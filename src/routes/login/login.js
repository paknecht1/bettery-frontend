import React, {useRef} from 'react';
import {
    Box,
    Button,
    CircularProgress,
    Flex,
    FormControl,
    FormErrorMessage,
    FormLabel,
    Heading,
    Input,
    Stack,
    useColorModeValue,
    useToast,
} from '@chakra-ui/react';
import * as Yup from 'yup';
import {Field, Form, Formik} from 'formik';
import axios from 'axios';
import {useHistory} from 'react-router-dom';

const Login = ({handleSuccessfulAuth}) => {
    const history = useHistory();
    const failToast = useToast();
    const toastFailRef = useRef();

    const successToast = useToast();
    const toastSuccessRef = useRef();

    let formFields = {
        username: '',
        password: '',
    };

    const ContactFormSchema = Yup.object().shape({
        username: Yup.string().required('Bitte geben sie Ihren Usernamen ein'),
        password: Yup.string().required('Bitte geben sie Ihr Passwort ein'),
    });

    const handleSubmit = (values, actions) => {
        axios
            .post(`/api/user/login?username=${values.username}&password=${values.password}`)
            .then(function (response) {
                // handle success
                toastSuccessRef.current = successToast({
                    title: 'Login successful',
                    description: 'You will be redirected shortly',
                    status: 'success',
                    duration: 6000,
                    isClosable: true,
                });
                actions.setSubmitting(false);
                handleSuccessfulAuth(response.data);
                setTimeout(function () {
                    history.push('/');
                }, 750);
            })
            .catch(function (error) {
                toastFailRef.current = failToast({
                    title: 'Login incorrect',
                    description: 'No matching login found with these credentials',
                    status: 'error',
                    duration: 6000,
                    isClosable: true,
                });
                actions.setSubmitting(false);
                actions.resetForm();
                console.log(error);
            });
    };

    return (
        <Flex
            minH={'100vh'}
            align={'center'}
            justify={'center'}
            bg={useColorModeValue('gray.50', 'gray.800')}
        >
            <Stack spacing={8} mx={'auto'} maxW={'lg'} py={12} px={6}>
                <Stack align={'center'}>
                    <Heading fontSize={'4xl'}>Sign in to your account</Heading>
                </Stack>
                <Box rounded={'lg'} bg={useColorModeValue('white', 'gray.700')} boxShadow={'lg'} p={8}>
                    <Stack spacing={4}>
                        <Formik
                            initialValues={formFields}
                            validationSchema={ContactFormSchema}
                            onSubmit={handleSubmit}
                        >
                            {(props) => (
                                <Form>
                                    <Field name="username">
                                        {({field, form}) => (
                                            <FormControl isInvalid={form.errors.username && form.touched.username}
                                                         mt={4}>
                                                <FormLabel htmlFor="username">Username</FormLabel>
                                                <Input
                                                    {...field}
                                                    type="text"
                                                    size="lg"
                                                    id="username"
                                                    placeholder="Username"
                                                />
                                                <FormErrorMessage>{form.errors.username}</FormErrorMessage>
                                            </FormControl>
                                        )}
                                    </Field>
                                    <Field name="password">
                                        {({field, form}) => (
                                            <FormControl isInvalid={form.errors.password && form.touched.password}
                                                         mt={4}>
                                                <FormLabel htmlFor="password">Name</FormLabel>
                                                <Input
                                                    {...field}
                                                    type="password"
                                                    size="lg"
                                                    id="password"
                                                    placeholder="Password"
                                                />
                                                <FormErrorMessage>{form.errors.password}</FormErrorMessage>
                                            </FormControl>
                                        )}
                                    </Field>
                                    <Stack spacing={10}>
                                        <Button
                                            type="submit"
                                            bg={'#00703d'}
                                            _hover={{bg: '#019654'}}
                                            mt={4}
                                        >
                                            {props.isSubmitting ? (
                                                <CircularProgress isIndeterminate size="24px" color="black"/>
                                            ) : (
                                                'Absenden'
                                            )}
                                        </Button>
                                    </Stack>
                                    <Stack spacing={10}>
                                        <Button
                                            onClick={() => history.push('/')}
                                            bg={'#8b928f'}
                                            _hover={{bg: '#777d7a'}}
                                            mt={4}
                                        >
                                            Back to Overview
                                        </Button>
                                    </Stack>
                                </Form>
                            )}
                        </Formik>
                    </Stack>
                </Box>
            </Stack>
        </Flex>
    );
};

export default Login;
