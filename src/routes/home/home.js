import React, {useEffect, useRef, useState} from 'react';
import {Button, Flex, Heading, Image, Stack, Text, useColorModeValue, useToast} from '@chakra-ui/react';
import {Link} from 'react-router-dom';
import Logo from '../../assets/logo_transparent.png';
import BetList from '../../components/betList';
import axios from 'axios';

const Home = ({user, userData, handleBetJoin}) => {

    const [bets, setBets] = useState([]);
    const failToast = useToast();
    const toastFailRef = useRef();

    const successToast = useToast();
    const toastSuccessRef = useRef();

    const loaUserBets = () => {
        if (user.loggedIn) {
            axios
                .get(`/api/bets/list`, {})
                .then(function (response) {
                    setBets(response.data.filter(item => item.open));
                })
                .catch(function (error) {
                });
        }
    };

    useEffect(() => {
        loaUserBets();
    }, [user, userData]);

    const handleClick = (bet) => {
        axios
            .put(`/api/bets/${bet.betId}/join`, {})
            .then(function (response) {
                console.log(response);
                toastSuccessRef.current = successToast({
                    title: 'Success',
                    description: 'We Wish you good luck',
                    status: 'success',
                    duration: 4000,
                    isClosable: true,
                });
                loaUserBets();
                handleBetJoin();
            })
            .catch(function (error) {
                if (error && error.response.status === 403) {
                    toastFailRef.current = failToast({
                        title: 'Can not join bet',
                        description: error.response.data,
                        status: 'error',
                        duration: 4000,
                        isClosable: true,
                    });
                } else {
                    toastFailRef.current = failToast({
                        title: 'Can not join bet',
                        description: 'Something went wrong',
                        status: 'error',
                        duration: 4000,
                        isClosable: true,
                    });
                }

            });
    };

    return (
        <div>
            {user.loggedIn && (
                <>
                    <Heading
                        fontWeight={600}
                        fontSize={{base: '1xl', sm: '2xl', md: '4xl'}}
                        lineHeight={'110%'}>
                        Welcome to BETtery
                    </Heading>
                    <BetList bets={bets} handleBetJoinRequest={handleClick} userData={userData}/>
                </>
            )}
            {!user.loggedIn && (
                <Flex
                    minH={'100vh'}
                    align={'center'}
                    justify={'center'}
                    bg={useColorModeValue('gray.50', 'gray.800')}
                >
                    <Stack
                        textAlign={'center'}
                        align={'center'}
                        spacing={{base: 8, md: 10}}
                        py={{base: 20, md: 28}}>
                        <Heading
                            fontWeight={600}
                            fontSize={{base: '3xl', sm: '4xl', md: '6xl'}}
                            lineHeight={'110%'}>
                            Bet placing{' '}
                            <Text as={'span'} color={'#00703d'}>
                                made easy
                            </Text>
                        </Heading>
                        <Text color={'gray.500'} maxW={'3xl'}>
                            May the odds be ever in your favor
                        </Text>
                        <Stack spacing={6} direction={'row'}>
                            <Link to="/login">
                                <Button
                                    rounded={'full'}
                                    px={6}
                                    bg={'#00703d'}
                                    _hover={{bg: '#019654'}}>
                                    Login
                                </Button>
                            </Link>
                            <Link to="/register">
                                <Button rounded={'full'} px={6}>
                                    Sign Up
                                </Button>
                            </Link>
                        </Stack>
                        <Image src={Logo}/>
                    </Stack>
                </Flex>
            )}
        </div>
    );
};

export default Home;
