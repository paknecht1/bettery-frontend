import React, {useEffect, useState} from 'react';
import BetList from '../../components/betList';
import axios from 'axios';
import {Heading} from '@chakra-ui/react';

const UserBets = ({userData}) => {

    const [myBets, setMyBets] = useState([]);

    const loaUserBets = () => {
        axios
            .get(`/api/user/bets`, {})
            .then(function (response) {
                setMyBets(response.data);
            })
            .catch(function (error) {
            });
    };

    useEffect(() => {
        loaUserBets();
    }, []);

    const handleClick = (bet) => {
        //TODO: Handle the bet join request
    };

    return (
        <>
            <Heading
                fontWeight={600}
                fontSize={{base: '1xl', sm: '2xl', md: '4xl'}}
                lineHeight={'110%'}>
                Your Bets
            </Heading>
            <BetList bets={myBets} handleBetJoinRequest={handleClick} userData={userData}/>
        </>
    );
};

export default UserBets;
