import React from 'react';
import {Text, Center, Image, Box, Flex, Avatar, Stack, Heading, useColorModeValue} from '@chakra-ui/react';
import Logo from '../../assets/jackpot.png';

const Profile = ({userData}) => {

    return (
        <div>
            <Center py={6}>
                <Box
                    maxW={'270px'}
                    w={'full'}
                    bg={useColorModeValue('white', 'gray.800')}
                    boxShadow={'2xl'}
                    rounded={'md'}
                    overflow={'hidden'}>
                    <Image
                        h={'120px'}
                        w={'full'}
                        src={Logo}
                        objectFit={'cover'}
                    />
                    <Box p={6}>
                        <Stack spacing={0} align={'center'} mb={5}>
                            <Heading fontSize={'2xl'} fontWeight={500} fontFamily={'body'}>
                                {userData.firstName} {userData.lastName}
                            </Heading>
                            <Text color={'gray.500'}>{userData.email}</Text>
                            <Text color={'gray.500'}>{userData.username}</Text>
                        </Stack>

                        <Stack direction={'row'} justify={'center'} spacing={6}>
                            <Stack spacing={0} align={'center'}>
                                <Text fontWeight={600}>{userData.balance}</Text>
                                <Text fontSize={'sm'} color={'gray.500'}>
                                    Coins
                                </Text>
                            </Stack>
                        </Stack>
                    </Box>
                </Box>
            </Center>
        </div>
    );
};

export default Profile;
