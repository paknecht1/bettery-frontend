# BETtery Frontend

# Guidelines

* install `NodeJs` Version `15.2.0`
* install `NPM` Version `7.0.10`
* install `yarn` Version `1.22.10`

# Dependencies

* axios (https://www.npmjs.com/package/axios) interacting with rest-interface
* formik (https://formik.org/docs/overview) form handling
* chakra ui (https://chakra-ui.com/) for styling
* react-router-dom (https://reactrouter.com/web/guides/philosophy) for routing

# Docker

Install Docker on your machine (https://docs.docker.com/get-docker/)

## Simple Build
`./build.sh`

To run the whole application simply checkout [BETtery Application](https://gitlab.com/paknecht1/bettery-application) and follow the instructions there.

# Development

## Work on client

### Without work on the Backend
Start the [BETtery Application](https://gitlab.com/paknecht1/bettery-application) as written there in the readme.

### With work on the Backend
Follow the instruction on [BETtery Backend](https://gitlab.com/paknecht1/bettery-backend) to start this server locally.

Then you can run the normal `yarn start` and start developing.


## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `yarn build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

## Acknowledgements

Thanks to https://chakra-templates.dev/ for providing nicely styled components to use.
